package io.gitlab.pixelGraphics.twoDimGraphics

import io.gitlab.pixelGraphics.twoDimGraphics.style.FillRule
import io.gitlab.pixelGraphics.twoDimGraphics.style.LineCap
import io.gitlab.pixelGraphics.twoDimGraphics.style.LineJoin
import io.gitlab.pixelGraphics.twoDimGraphics.surface.Surface
import io.gitlab.pixelGraphics.twoDimGraphics.text.Text

/**
 * Provides 2D (two dimensional) graphics drawing functionality that is applied to a [Surface].
 */
public expect class Canvas : Closable {
    /**
     * The current line join style for the [Canvas]. As with the other stroke parameters the current line join style is
     * examined by [stroke], and [strokeExtents] but doesn't have any effect
     * during path construction. The default line join style is [LineJoin.MITER].
     * @see LineJoin
     */
    public var lineJoin: LineJoin
    /**
     * The current line cap style for the [Canvas]. As with the other stroke parameters the current line cap style is
     * examined by [stroke], and [strokeExtents] but doesn't have any effect
     * during path construction. The default line cap style is [LineCap.BUTT].
     * @see LineCap
     */
    public var lineCap: LineCap
    /**
     * The current fill rule for the [Canvas]. The fill rule is used to determine which regions are inside, or
     * outside a complex (potentially self-intersecting) path. The current fill rule affects both [fill], and
     * `cairo_clip`. The default fill rule is [FillRule.WINDING].
     * @see FillRule
     */
    public var fillRule: FillRule
    /** The length of the dash array, or *0* if no dash array is set. */
    public val dashCount: Int
    /**
     * The current line width for the [Canvas]. This property specifies the diameter of a pen that is
     * circular in user space, (though device-space pen may be an ellipse in general due to scaling/shear/rotation of
     * the CTM). **Note:** When the description above refers to user space and CTM it refers to the user space, and
     * CTM in effect at the time of the stroking operation, **NOT** the user space and CTM in effect at the time of the
     * call to the [lineWidth] property. The simplest usage makes both of these spaces identical. That is if there is
     * no change to the CTM between a call to the [lineWidth] property and the stroking operation, then one can just
     * pass user-space values to the [lineWidth] property and ignore this note.
     *
     * As with the other stroke parameters the current line width is examined by [stroke], and [strokeExtents] but
     * doesn't have any effect during path construction. The default line width value is *2.0*.
     */
    public var lineWidth: Double
    /**
     * The current miter limit for the [Canvas]. If the current line join style is set to [LineJoin.MITER] then the
     * miter limit is used to determine whether the lines should be joined with a bevel instead of a miter. Cairo
     * divides the length of the miter by the line width. If the result is greater than the miter limit then the style
     * is converted to a bevel.
     *
     * As with the other stroke parameters the current line miter limit is examined by [stroke], and [strokeExtents]
     * but doesn't have any effect during path construction. The default miter limit value is *10.0*, which will
     * convert joins with interior angles less than 11 degrees to bevels instead of miters. For reference a miter limit
     * of *2.0* makes the miter cutoff at 60 degrees, and a miter limit of *1.414* makes the cutoff at 90 degrees.
     *
     * A miter limit for a desired angle can be computed as: miter limit = 1/sin(angle/2)
     * @see lineJoin
     */
    public var miterLimit: Double
    /**
     * The tolerance (in device units; typically pixels) used when converting paths into trapezoids. Curved segments
     * of the path will be subdivided until the maximum deviation between the original path, and the polygonal
     * approximation is less than tolerance. The default value is *0.1*. A larger value will give better performance,
     * a smaller value, better appearance. Reducing the value from the default value of *0.1* is unlikely to improve
     * appearance significantly. The accuracy of paths within Cairo is limited by the precision of its internal
     * arithmetic, and the prescribed tolerance is restricted to the smallest representable internal value.
     */
    public var tolerance: Double

    /**
     * This is a convenience function for creating a pattern from [surface] and setting it as the source in [Canvas]
     * with `cairo_set_source`. The [xPos] and [yPos] parameters give the user-space coordinate at which the surface
     * origin should appear. (The surface origin is its upper-left corner before any transformation has been applied.)
     * The [xPos] and [yPos] parameters are negated, and then set as translation values in the pattern matrix.
     *
     * Other than the initial translation pattern matrix as described above, all other pattern attributes, such as its
     * extend mode, are set to the default values as in `cairo_pattern_create_for_surface`. The resulting pattern can
     * be queried with `cairo_get_source` so that these attributes can be modified if desired, (eg. to create a
     * repeating pattern with `cairo_pattern_set_extend`).
     * @param surface A surface to be used to set the source pattern.
     * @param xPos User-space X coordinate for surface origin.
     * @param yPos User-space Y coordinate for surface origin.
     */
    public fun changeSourceSurface(surface: Surface, xPos: Double, yPos: Double)

    /**
     * A DSL to use the [Canvas], which is closed along with its associated [Surface] after [init] has been called.
     * @param init A lambda for executing functions, and accessing/changing properties from the [Canvas] object.
     */
    public fun use(init: Canvas.() -> Unit)

    /**
     * Changes the source pattern to an opaque color. This opaque color will then be used for any subsequent drawing
     * operation until a new source pattern is set. The color components are floating point numbers in the range *0* to
     * *1*. If the values passed in are outside that range then they will be clamped. The default source pattern is
     * opaque black, which is equivalent to the following:
     * ```kotlin
     * changeSourceRgb(red = 0.0, green = 0.0, blue = 0.0)
     * ```
     * @param red The red colour to use.
     * @param green The green colour to use.
     * @param blue The blue colour to use.
     */
    public fun changeSourceRgb(red: Double, green: Double, blue: Double)

    /**
     * Sets the source pattern to a translucent color. This color will then be used for any subsequent drawing
     * operation until a new source pattern is set. The color, and alpha components are floating point numbers in the
     * range *0* to *1*. If the values passed in are outside that range then they will be clamped. The default source
     * pattern is opaque black, which is equivalent to the following:
     * ```kotlin
     * changeSourceRgba(red = 0.0, green = 0.0, blue = 0.0, alpha = 1.0)
     * ```
     * @param red The red colour to use.
     * @param green The green colour to use.
     * @param blue The blue colour to use.
     */
    public fun changeSourceRgba(red: Double, green: Double, blue: Double, alpha: Double)

    /**
     * Changes the dash pattern to be used by [stroke]. A dash pattern is specified by [dashes]. Each value
     * provides the length of alternate **on**, and **off** portions of the stroke. The [offset] specifies an offset
     * into the pattern at which the stroke begins. Each **on** segment will have caps applied as if the segment were
     * a separate sub-path. In particular it is valid to use an **on** length of *0.0* with `CAIRO_LINE_CAP_ROUND`, or
     * `CAIRO_LINE_CAP_SQUARE` in order to distributed dots or squares along a path.
     *
     * **Note:** The length values are in user-space units as evaluated at the time of stroking. This is not
     * necessarily the same as the user space at the time of [changeDash]. If [dashes] is empty then dashing is
     * disabled, however if [dashes] only contains a single element then a symmetric pattern is assumed with
     * alternating on, and off portions of the size specified by the single value in dashes.
     *
     * If any value in [dashes] is negative, or if all values are *0* then [Canvas] will be put into an error state
     * with a status of `CAIRO_STATUS_INVALID_DASH`.
     * @param dashes An array specifying alternate lengths of on, and off stroke portions.
     * @param offset An offset into the dash pattern at which the stroke should start.
     */
    public fun changeDash(dashes: DoubleArray, offset: Double)

    /**
     * Gets the current dash array. If not *null* then dashes should be big enough to hold at least the number of
     * values returned by the [dashCount] property.
     * @return A Pair containing the following:
     * 1. dashes - The dash array (may be empty).
     * 2. offset - The current dash offset.
     */
    public fun fetchDash(): Pair<DoubleArray, Double>

    public companion object {
        /**
         * Creates the [Canvas] with an associated [surface].
         * @param surface The surface that the canvas is associated with.
         */
        public fun create(surface: Surface): Canvas
    }

    /** Provides the status code for this [Canvas].  */
    public fun statusCode(): UInt

    /**
     * A drawing operator that fills the current path according to the current fill rule, (each sub-path is implicitly
     * closed before being filled). After [fill] the current path will be cleared from the [Canvas].
     * @see fillPreserve
     */
    public fun fill()

    /**
     * A drawing operator that fills the current path according to the current fill rule, (each sub-path is implicitly
     * closed before being filled). Unlike [fill], [fillPreserve] preserves the path within the cairo context.
     * @see fill
     */
    public fun fillPreserve()

    /**
     * Computes a bounding box in user coordinates covering the area that would be affected (the "inked" area), by a
     * [fill] operation given the current path and fill parameters. If the current path is empty, returns an empty
     * rectangle ((0,0), (0,0)). Surface dimensions and clipping are not taken into account.
     * @return The result of the operation.
     * @see fill
     * @see fillPreserve
     */
    public fun fillExtents(): FillExtentsResult

    /**
     * Tests whether the given point is inside the area that would be affected by a [fill] operation given the current
     * path and filling parameters. Surface dimensions and clipping are not taken into account.
     * @param xPos X coordinate of the point to test.
     * @param yPos Y coordinate of the point to test.
     * @return A value of *true* if the point is inside.
     * @see fill
     * @see fillPreserve
     */
    public fun inFill(xPos: Double, yPos: Double): Boolean

    /**
     * A drawing operator that strokes the current path according to the current line width, line join, line cap, and
     * dash settings. After [stroke] the current path will be cleared from the [Canvas]. **Note:** Degenerate segments
     * and sub-paths are treated specially, and provide a useful result. These can result in two different situations:
     * - Zero-length **on** segments set via [changeDash]. If the cap style is [LineCap.ROUND], or
     * [LineCap.SQUARE] then these segments will be drawn as circular dots or squares respectively. In the case
     * of [LineCap.SQUARE] the orientation of the squares is determined by the direction of the underlying path.
     * - A sub-path created by [Path.moveTo] followed by either a `cairo_close_path`, or one or more calls to
     * [Path.lineTo] to the same coordinate as [Path.moveTo]. If the cap style is [LineCap.ROUND] then these
     * sub-paths will be drawn as circular dots. Note that in the case of  [LineCap.SQUARE] a degenerate
     * sub-path will not be drawn at all (since the correct orientation is indeterminate).
     *
     * In no case will a cap style of [LineCap.BUTT] cause anything to be drawn in the case of either
     * degenerate segments or sub-paths.
     * @see strokePreserve
     */
    public fun stroke()

    /**
     * A drawing operator that strokes the current path according to the current line width, line join, line cap, and
     * dash settings. Unlike [stroke], [strokePreserve] preserves the path within the [Canvas].
     */
    public fun strokePreserve()

    /**
     * Computes a bounding box in user coordinates covering the area that would be affected, (the **inked** area), by a
     * [stroke] operation given the current path and stroke parameters. If the current path is empty, returns an empty
     * rectangle ((0,0), (0,0)). Surface dimensions and clipping are not taken into account. Note that if the line
     * width is set to exactly zero, then [strokeExtents] will return an empty rectangle.
     * @return The result of the operation.
     * @see stroke
     * @see strokePreserve
     */
    public fun strokeExtents(): StrokeExtentsResult

    /**
     * Tests whether the given point is inside the area that would be affected by a [stroke] operation given the
     * current path and stroking parameters. Surface dimensions and clipping are not taken into account.
     * @param xPos X coordinate of the point to test.
     * @param yPos Y coordinate of the point to test.
     * @return A value of *true* if the point is inside.
     * @see stroke
     * @see strokePreserve
     */
    public fun inStroke(xPos: Double, yPos: Double): Boolean

    /**
     * Emits the current page for backends that support multiple pages, but doesn't clear it so the contents of the
     * current page will be retained for the next page too. Use [showPage] if you want to get an empty page after the
     * emission.
     */
    public fun copyPage()

    /**
     * Emits and clears the current page for backends that support multiple pages. Use [copyPage] if you don't want to
     * clear the page.
     */
    public fun showPage()
}

public fun Canvas.createPath(init: Path.() -> Unit): Path {
    val result = Path(this)
    result.init()
    return result
}

public fun Canvas.createText(init: Text.() -> Unit): Text {
    val result = Text(this)
    result.init()
    return result
}

public fun Canvas.createTransformation(init: Transformation.() -> Unit): Transformation {
    val result = Transformation(this)
    result.init()
    return result
}
