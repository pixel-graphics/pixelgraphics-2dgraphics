package io.gitlab.pixelGraphics.twoDimGraphics

/**
 * The sub pixel order specifies the order of color elements within each pixel on the display device when rendering
 * with an anti aliasing mode of [AntiAliasType.SUBPIXEL].
 */
public enum class SubPixelOrder {
    /** Use the default subpixel order for for the target device */
    DEFAULT,
    /** Subpixel elements are arranged horizontally with red at the left. */
    RGB,
    /** Subpixel elements are arranged horizontally with blue at the left. */
    BGR,
    /** Subpixel elements are arranged vertically with red at the top. */
    VRGB,
    /** Unrecognized sub pixel order. */
    UNKNOWN
}
