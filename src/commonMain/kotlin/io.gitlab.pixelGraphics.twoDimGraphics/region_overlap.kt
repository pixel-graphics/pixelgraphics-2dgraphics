package io.gitlab.pixelGraphics.twoDimGraphics

/** Indicates where the region contents are positioned. */
public enum class RegionOverlap {
    /** The contents are entirely inside the region. */
    IN,
    /** The contents are entirely outside the region. */
    OUT,
    /** The contents are partially inside and partially outside the region. */
    PART,
    /** Unrecognised region overlap. */
    UNKNOWN
}
