package io.gitlab.pixelGraphics.twoDimGraphics.surface

public data class InkExtentsResult(val xPos: Double, val yPos: Double, val width: Double, val height: Double)
