package io.gitlab.pixelGraphics.twoDimGraphics.surface

/**
 * Uses a PDF as the destination for the 2D (two dimensional) graphics.
 */
public expect class PdfSurface : Surface {
    public companion object {
        /**
         * Creates a [PdfSurface] of the specified size in points to be written to [filePath].
         * @param width Width of the surface in points.
         * @param height Width of the surface in points.
         * @return An [PdfSurface] or an IllegalStateException if the surface can't be created.
         */
        public fun create(filePath: String, width: Double, height: Double): PdfSurface
    }

    /**
     * Changes the size of a [PdfSurface] for the current (and subsequent) pages. This function should **ONLY** be
     * called **before** any drawing operations have been performed on the current page. The simplest way to do this is
     * to call this function immediately **after** creating the surface, or immediately **after** completing a page
     * with either cairo_show_page() or cairo_copy_page().
     * @param width New surface width in points.
     * @param height New surface height in points.
     */
    public fun changeSize(width: Double, height: Double)
}
