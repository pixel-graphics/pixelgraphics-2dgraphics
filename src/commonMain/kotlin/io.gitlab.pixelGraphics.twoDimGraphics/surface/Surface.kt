package io.gitlab.pixelGraphics.twoDimGraphics.surface

import io.gitlab.pixelGraphics.twoDimGraphics.Closable

/**
 * Represents the destination for the 2D (two dimensional) graphics.
 */
public expect interface Surface : Closable
