package io.gitlab.pixelGraphics.twoDimGraphics.surface

/**
 * Uses an image as the destination for the 2D (two dimensional) graphics.
 */
public expect class ImageSurface : Surface {
    public companion object {
        /**
         * Creates an [ImageSurface].
         * @param format The image format to use.
         * @param width The size of the image in pixels.
         * @param height The size of the image in pixels.
         * @return An [ImageSurface] or an IllegalStateException if the surface can't be created.
         */
        public fun create(format: Int, width: Int, height: Int): ImageSurface

        /**
         * Creates a new [ImageSurface] and initializes the contents to the given PNG file.
         * @param filePath The path to the PNG file to load.
         * @return A Pair containing the following:
         * 1. Surface - A new [ImageSurface] initialized with the contents of the PNG file, or *null* if an error
         * occurred.
         * 2. Status - Will be *0* on success, or a higher number if an error occurred.
         */
        public fun fromPngFile(filePath: String): Pair<ImageSurface?, UInt>
    }

    /**
     * Writes all graphics data to a PNG file.
     * @param filePath The path to the PNG file.
     * @return The status of the operation. Will be *0* on success, or a higher number if an error occurred.
     */
    public fun writeToPngFile(filePath: String): UInt
}
