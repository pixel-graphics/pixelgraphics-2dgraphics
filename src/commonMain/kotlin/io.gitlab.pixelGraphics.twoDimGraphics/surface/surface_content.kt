package io.gitlab.pixelGraphics.twoDimGraphics.surface

/**
 * Is used to describe the content that a [Surface] will contain, whether color information, alpha information
 * (translucence vs. opacity), or both.
 */
public enum class SurfaceContent {
    /** The surface will hold color content only. */
    COLOR,
    /** The surface will hold alpha content only. */
    ALPHA,
    /** The surface will hold color and alpha content. */
    COLOR_ALPHA,
    /** Unrecognised surface content. */
    UNKNOWN
}
