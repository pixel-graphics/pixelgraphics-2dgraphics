package io.gitlab.pixelGraphics.twoDimGraphics.surface

/** Uses SVG as the destination for the 2D (two dimensional) graphics. */
public expect class SvgSurface : Surface {
    public companion object {
        /**
         * Creates a SVG surface of the specified size in points to be written to [filePath]. The SVG surface backend
         * recognizes the following MIME types for the data attached to a surface (see `cairo_surface_set_mime_data`)
         * when it is used as a source pattern for drawing on this surface: `CAIRO_MIME_TYPE_JPEG`,
         * `CAIRO_MIME_TYPE_PNG`, `CAIRO_MIME_TYPE_URI`. If any of them is specified then the SVG backend emits a
         * href with the content of MIME data instead of a surface snapshot (PNG, Base64-encoded) in the corresponding
         * image tag.
         *
         * The unofficial MIME type `CAIRO_MIME_TYPE_URI` is examined first. If present the URI is emitted as is:
         * assuring the correctness of URI is left to the client code. If `CAIRO_MIME_TYPE_URI` is not present, but
         * `CAIRO_MIME_TYPE_JPEG` or `CAIRO_MIME_TYPE_PNG` is specified then the corresponding data is Base64-encoded,
         * and emitted.
         *
         * If `CAIRO_MIME_TYPE_UNIQUE_ID` is present then all surfaces with the same unique identifier will only be
         * embedded once.
         * @param filePath A file path for the SVG output (must be writable), *""* (an empty [String]) may be used to
         * specify no output. This will generate a SVG surface that may be queried and used as a source, without
         * generating a temporary file.
         * @param width Width of the surface in points.
         * @param height Height of the surface in points.
         * @return The newly created surface. Remember to call [close] when finished with the surface.
         */
        public fun create(filePath: String = "", width: Double, height: Double): SvgSurface
    }
}