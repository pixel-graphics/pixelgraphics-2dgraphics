package io.gitlab.pixelGraphics.twoDimGraphics.surface

import io.gitlab.pixelGraphics.twoDimGraphics.DoubleRectangle

/** Records all drawing operations. */
public expect class RecordingSurface : Surface {
    public companion object {
        /**
         * Creates a recording surface which can be used to record all drawing operations at the highest level
         * (that is the level of paint, mask, stroke, fill and show_text_glyphs). The recording surface can then be
         * **replayed** against any target surface by using it as a source to drawing operations. The recording phase
         * of the recording surface is careful to snapshot all necessary objects (paths, patterns, etc.), in order to
         * achieve accurate replay.
         * @param extents The extents to record in pixels, can be *null* to record unbounded operations.
         * @return The newly created surface. Remember to [close] the object when finished with it.
         */
        public fun create(
            surfaceContent: SurfaceContent = SurfaceContent.COLOR_ALPHA,
            extents: DoubleRectangle? = null
        ): RecordingSurface
    }

    /**
     * Measures the extents of the operations stored within the recording-surface. This is useful to compute the
     * required size of an image surface (or equivalent) into which to replay the full sequence of drawing operations.
     */
    public fun inkExtents(): InkExtentsResult
}
