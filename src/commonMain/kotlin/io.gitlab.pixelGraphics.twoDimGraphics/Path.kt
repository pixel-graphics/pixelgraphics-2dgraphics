package io.gitlab.pixelGraphics.twoDimGraphics

/**
 * Creates paths, and manipulates path data.
 * @param canvas The canvas used by [Path].
 */
public expect class Path(canvas: Canvas) {
    /** Whether a current point is defined on the current path. */
    public val hasCurrentPoint: Boolean

    /**
     * Gets the current point of the current path, which is conceptually the final point reached by the path so
     * far. The current point is returned in the user-space coordinate system. If there is no defined current
     * point or if cr is in an error status then xPos and yPos will both be set to *0.0*. It is possible to check
     * this in advance with [hasCurrentPoint]. Most path construction functions alter the current point. See the
     * following for details on how they affect the current point: [cairo_rel_move_to], [cairo_rel_line_to],
     * [cairo_rel_curve_to], [cairo_text_path], [cairo_glyph_path].
     *
     * Some functions use and alter the current point but do not otherwise change current path line like
     * [cairo_show_text]. Other functions unset the current path and as a result, current point: [Canvas.fill],
     * [Canvas.stroke]
     * @see newPath
     * @see newSubPath
     * @see moveTo
     * @see lineTo
     * @see curveTo
     * @see arc
     * @see arcNegative
     * @see rectangle
     * @see closePath
     * @return A Pair containing the following:
     * 1. xPos - X coordinate of the current point.
     * 2. yPos - Y coordinate of the current point.
     */
    public fun fetchCurrentPoint(): Pair<Double, Double>

    /**
     * Clears the current path. After this call there will be no path and no current point.
     */
    public fun newPath()

    /**
     * Begin a new sub-path. Note that the existing path is not affected. After this call there will be no current
     * point. In many cases this call is not needed since new sub-paths are frequently started with [moveTo]. A
     * call to [newSubPath] is particularly useful when beginning a new sub-path with one of the [arc] calls. This
     * makes things easier as it is no longer necessary to manually compute the arc's initial coordinates for a
     * call to [moveTo].
     */
    public fun newSubPath()

    /**
     * Adds a line segment to the path from the current point to the beginning of the current sub-path, (the most
     * recent point passed to [moveTo]), and closes this sub-path. After this call the current point will be at
     * the joined endpoint of the sub-path. The behavior of [closePath] is distinct from simply calling
     * [lineTo] with the equivalent coordinate in the case of stroking. When a closed sub-path is stroked there are
     * no caps on the ends of the sub-path. Instead there is a line join connecting the final, and initial segments
     * of the sub-path.
     *
     * If there is no current point before the call to [closePath] then this function will have no effect.
     * **Note:** As of cairo version 1.2.4 any call to [closePath] will place an explicit `MOVE_TO` element into
     * the path immediately after the `CLOSE_PATH` element, (which can be seen in [cairo_copy_path] for example).
     * This can simplify path processing in some cases as it may not be necessary to save the
     * **last move_to point** during processing as the `MOVE_TO` immediately after the `CLOSE_PATH` will provide
     * that point.
     */
    public fun closePath()

    /**
     * Adds a circular arc of the given radius to the current path. The arc is centered at (xCenterPos,
     * yCenterPos), begins at [angle1] and proceeds in the direction of increasing angles to end at [angle2]. If
     * [angle2] is less than [angle1] it will be progressively increased by 2*M_PI until it is greater than
     * [angle1]. If there is a current point, an initial line segment will be added to the path to connect the
     * current point to the beginning of the arc. If this initial line is undesired then it can be avoided by
     * calling [newSubPath] before calling [arc].
     *
     * Angles are measured in radians. An angle of *0.0* is in the direction of the positive X axis (in user
     * space). An angle of M_PI/2.0 radians (90 degrees) is in the direction of the positive Y axis (in user
     * space). Angles increase in the direction from the positive X axis toward the positive Y axis. So with the
     * default transformation matrix, angles increase in a clockwise direction. To convert from degrees to radians,
     * use degrees * (M_PI / 180).
     *
     * This function gives the arc in the direction of increasing angles; see [arcNegative] to get the arc in the
     * direction of decreasing angles. The arc is circular in user space. To achieve an elliptical arc, you can
     * scale the current transformation matrix by different amounts in the X and Y directions.
     * @param xCenterPos X position of the center of the arc.
     * @param yCenterPos Y position of the center of the arc.
     * @param radius The radius of the arc.
     * @param angle1 The start angle in radians.
     * @param angle2 The end angle in radians.
     */
    public fun arc(
        xCenterPos: Double,
        yCenterPos: Double,
        radius: Double,
        angle1: Double,
        angle2: Double
    )

    /**
     * Adds a circular arc of the given radius to the current path. The arc is centered at (xCenterPos ,
     * yCenterPos), begins at [angle1] and proceeds in the direction of decreasing angles to end at [angle2]. If
     * [angle2] is greater than [angle1] it will be progressively decreased by 2*M_PI until it is less than
     * [angle1].
     * @param xCenterPos X position of the center of the arc.
     * @param yCenterPos Y position of the center of the arc.
     * @param radius The radius of the arc.
     * @param angle1 The start angle in radians.
     * @param angle2 The end angle in radians.
     * @see arc
     */
    public fun arcNegative(
        xCenterPos: Double,
        yCenterPos: Double,
        radius: Double,
        angle1: Double,
        angle2: Double
    )

    /**
     * Adds a cubic Bézier spline to the path from the current point to position (x3, y3) in user-space
     * coordinates, using (x1, y1) and (x2, y2) as the control points. After this call the current point will be
     * (x3, y3). If there is no current point before the call to [curveTo] then this function will behave as if
     * preceded by a call to moveTo(cr , x1 , y1).
     * @param x1 The X coordinate of the first control point.
     * @param y1 The Y coordinate of the first control point.
     * @param x2 The X coordinate of the second control point.
     * @param y2 The Y coordinate of the second control point.
     * @param x3 the X coordinate of the end of the curve.
     * @param y3 the Y coordinate of the end of the curve.
     */
    public fun curveTo(x1: Double, y1: Double, y2: Double, x2: Double, x3: Double, y3: Double)

    /**
     * Adds a line to the path from the current point to position (xPos, yPos) in user-space coordinates. After
     * this call the current point will be (xPos, yPos). If there is no current point before the call to
     * [lineTo] then this function will behave as moveTo(cr , x , y ).
     * @param xPos The X coordinate of the end of the new line.
     * @param yPos The Y coordinate of the end of the new line.
     */
    public fun lineTo(xPos: Double, yPos: Double)

    /**
     * Begin a new sub-path. After this call the current point will be (xPos, yPos).
     * @param xPos The X coordinate of the new position.
     * @param yPos The Y coordinate of the new position.
     */
    public fun moveTo(xPos: Double, yPos: Double)

    /**
     * Adds a closed sub-path rectangle of the given size to the current path at position (xPos, yPos) in
     * user-space coordinates.
     * @param xPos The X coordinate of the top left corner of the rectangle.
     * @param yPos The Y coordinate of the top left corner of the rectangle.
     * @param width The width of the rectangle.
     * @param height The height of the rectangle.
     */
    public fun rectangle(xPos: Double, yPos: Double, width: Double, height: Double)
}
