package io.gitlab.pixelGraphics.twoDimGraphics

import io.gitlab.pixelGraphics.twoDimGraphics.surface.Surface

/**
 * Converts the [status code][statusCode] into a readable [String].
 * @param statusCode The status code to convert.
 * @return The [String] representation of [statusCode].
 */
public expect fun statusCodeToString(statusCode: UInt): String

/**
 * Fetches the status code for the [surface].
 * @param surface The [Surface] to use.
 * @return The status code for the [surface].
 */
public expect fun surfaceStatusCode(surface: Surface): UInt
