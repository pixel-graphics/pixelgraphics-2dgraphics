package io.gitlab.pixelGraphics.twoDimGraphics

/** Holds data for a rectangle. */
public expect class DoubleRectangle : Closable {
    /** X coordinate of the left side of the rectangle. */
    public var xPos: Double

    /** Y coordinate of the the top side of the rectangle. */
    public var yPos: Double

    /** Width of the rectangle. */
    public var width: Double

    /** Height of the rectangle. */
    public var height: Double

    public companion object {
        public fun create(
            newXPos: Double = 0.0,
            newYPos: Double = 0.0,
            newWidth: Double = 0.0,
            newHeight: Double = 0.0
        ): DoubleRectangle
    }
}
