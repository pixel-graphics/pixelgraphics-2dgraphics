package io.gitlab.pixelGraphics.twoDimGraphics

/**
 * Provides the result of the strokeExtents function from [Canvas].
 * @param x1 Left of the resulting extents.
 * @param y1 Top of the resulting extents.
 * @param x2 Right of the resulting extents.
 * @param y2 Bottom of the resulting extents.
 */
public data class StrokeExtentsResult(val x1: Double, val y1: Double, val x2: Double, val y2: Double)
