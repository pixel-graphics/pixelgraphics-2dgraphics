package io.gitlab.pixelGraphics.twoDimGraphics.style

/** Specifies how to render the junction of two lines when stroking. The default line join style is [MITER] */
public enum class LineJoin {
    /** Use a sharp (angled) corner. */
    MITER,
    /** Use a rounded join. The center of the circle is the joint point. */
    ROUND,
    /** Use a cut-off join. The join is cut off at half the line width from the joint point. */
    BEVEL,
    /** Unrecognized line join. */
    UNKNOWN
}
