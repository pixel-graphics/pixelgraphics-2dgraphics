package io.gitlab.pixelGraphics.twoDimGraphics.style

/** Specifies how to render the endpoints of the path when stroking. The default line cap style is [BUTT]. */
public enum class LineCap {
    /** Start (stop) the line exactly at the start (end) point. */
    BUTT,
    /** Use a round ending. The center of the circle is the end point. */
    ROUND,
    /** Use squared ending. The center of the square is the end point. */
    SQUARE,
    /** Unrecognized line cap. */
    UNKNOWN
}
