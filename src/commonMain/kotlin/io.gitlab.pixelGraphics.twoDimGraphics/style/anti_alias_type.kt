package io.gitlab.pixelGraphics.twoDimGraphics.style

/**
 * Specifies the type of antialiasing to do when rendering text or shapes. There is no guarantee on how the backend
 * will perform its rasterisation (if it even rasterises!), nor that they have any differing effect other than to
 * enable some form of antialiasing. The interpretation of [DEFAULT] is left entirely up to the backend.
 */
public enum class AntiAliasType {
    /** Use the default antialiasing for the subsystem and target device. */
    DEFAULT,
    /** Use a bilevel alpha mask. */
    NONE,
    /** Perform single-color antialiasing (using shades of gray for black text on a white background, for example). */
    GRAY,
    /** Perform antialiasing by taking advantage of the order of subpixel elements on devices such as LCD panels. */
    SUBPIXEL,
    /** The anti alias type is unrecognized. */
    UNKNOWN
}
