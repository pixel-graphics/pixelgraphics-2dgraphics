package io.gitlab.pixelGraphics.twoDimGraphics

public interface Closable {
    public val isClosed: Boolean

    public fun close()
}
