package io.gitlab.pixelGraphics.twoDimGraphics

/**
 * Are a simple graphical data type representing an area of integer-aligned rectangles. They are often used on raster
 * surfaces to track areas of interest, such as change or clip areas.
 */
public expect class Region : Closable {
    /** Whether region is empty. */
    public val isEmpty: Boolean

    /** The number of rectangles contained in this [Region]. */
    public val totalRectangles: Int

    /** Gets the bounding rectangle for this [Region]. */
    public val extents: IntRectangle

    /**
     * Checks whether (xPos, yPos) is contained in this [Region].
     * @param xPos The X coordinate of a point.
     * @param yPos The Y coordinate of a point.
     * @return A value of *true* if the point is contained in this [Region].
     */
    public fun containsPoint(xPos: Int, yPos: Int): Boolean

    /**
     * Gets a rectangle from this [Region].
     * @param num A number indicating which rectangle should be returned.
     * @return The rectangle.
     */
    public fun fetchRectangle(num: Int): IntRectangle

    /** Provides the status code for this [Region]. */
    public fun statusCode(): UInt

    /**
     * Checks whether [rect] is inside, outside or partially contained in this [Region].
     * @param rect The rectangle to use for the check.
     * @return The region overlap type.
     */
    public fun containsRectangle(rect: IntRectangle): RegionOverlap

    /**
     * Translates this [Region] by (xPos, yPos).
     * @param xPos Amount to translate in the x direction.
     * @param yPos Amount to translate in the y direction.
     */
    public fun translate(xPos: Int, yPos: Int)

    /**
     * Computes the intersection of this [Region] with the [other region][other], and returns the result.
     * @param other The other region to use for the intersection.
     * @return A Pair containing the following:
     * 1. statusCode The status code of the operation.
     * 2. intersectionValue The value of the intersection.
     */
    public fun intersectRegion(other: Region): Pair<UInt, Region>

    /**
     * Computes the intersection of this [Region] with the [rectangle][rect], and returns the result.
     * @param rect The rectangle to use for the intersection.
     * @return A Pair containing the following:
     * 1. statusCode The status code of the operation.
     * 2. intersectionValue The value of the intersection.
     */
    public fun intersectRectangle(rect: IntRectangle): Pair<UInt, Region>

    /**
     * Subtracts the [other region][other] from this [Region] and returns the result.
     * @param other The region to subtract from this [Region].
     * @return A Pair containing the following:
     * 1. statusCode The status code of the operation.
     * 2. subtractValue The value of the subtract.
     */
    public fun subtractRegion(other: Region): Pair<UInt, Region>

    /**
     * Subtracts [rectangle][rect] from this [Region] and returns the result.
     * @param rect The rectangle to subtract from this [Region].
     * @return A Pair containing the following:
     * 1. statusCode The status code of the operation.
     * 2. subtractValue The value of the subtract.
     */
    public fun subtractRectangle(rect: IntRectangle): Pair<UInt, Region>

    /**
     * Computes the union of this [Region] with the [other region][other] and returns the result.
     * @param other The region to use.
     * @return A Pair containing the following:
     * 1. statusCode The status code of the operation.
     * 2. unionValue The value of the union.
     */
    public fun regionUnion(other: Region): Pair<UInt, Region>

    /**
     * Computes the union of this [Region] with the [rectangle][rect] and returns the result.
     * @param rect The rectangle to use.
     * @return A Pair containing the following:
     * 1. statusCode The status code of the operation.
     * 2. unionValue The value of the union.
     */
    public fun rectangleUnion(rect: IntRectangle): Pair<UInt, Region>

    /**
     * Computes the exclusive difference of this [Region] with the [other region][other], and returns the result. That
     * is xorValue will be set to contain all areas that are either in this [Region] or in the [other region][other],
     * but not in both.
     * @param other The region to use.
     * @return A Pair containing the following:
     * 1. statusCode The status code of the operation.
     * 2. xorValue The value of the xor.
     */
    public fun xorRegion(other: Region): Pair<UInt, Region>

    /**
     * Computes the exclusive difference of this [Region] with the [rectangle][rect], and returns the result. That
     * is xorValue will be set to contain all areas that are either in this [Region] or in the [rectangle][rect],
     * but not in both.
     * @param rect The rectangle to use.
     * @return A Pair containing the following:
     * 1. statusCode The status code of the operation.
     * 2. xorValue The value of the xor.
     */
    public fun xorRectangle(rect: IntRectangle): Pair<UInt, Region>

    public companion object {
        /**
         * Creates a new [Region].
         * @return A newly created region. Remember to [close] the object when finished with it.
         */
        public fun create(): Region

        /**
         * Creates a new [Region] with a rectangle.
         * @param rect The rectangle to use.
         * @return A newly created region containing a [rectangle][rect]. Remember to [close] the object when finished
         * with it.
         */
        public fun createWithRectangle(rect: IntRectangle): Region

        /**
         * Creates a new [Region] containing the union of all given [rectangles].
         * @param rectangles The rectangles to use. Can be safely [closed][IntRectangle.close] after calling this
         * function since it makes a deep copy of the parameter.
         * @return A newly created region containing multiple [rectangles]. Remember to [close] the object when
         * finished with it.
         */
        public fun createWithMultipleRectangles(vararg rectangles: IntRectangle): Region
    }
}

public fun region(vararg rectangles: IntRectangle, init: Region.() -> Unit = {}): Region {
    val result = when {
        rectangles.isEmpty() -> Region.create()
        rectangles.size == 1 -> Region.createWithRectangle(rectangles.first())
        else -> Region.createWithMultipleRectangles(*rectangles)
    }
    result.init()
    return result
}
