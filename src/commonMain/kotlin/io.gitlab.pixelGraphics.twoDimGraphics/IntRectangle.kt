package io.gitlab.pixelGraphics.twoDimGraphics

/** Holds data for a rectangle. */
public expect class IntRectangle : Closable {
    /** X coordinate of the left side of the rectangle. */
    public var xPos: Int

    /** Y coordinate of the the top side of the rectangle. */
    public var yPos: Int

    /** Width of the rectangle. */
    public var width: Int

    /** Height of the rectangle. */
    public var height: Int

    public companion object {
        public fun create(
            newXPos: Int = 0,
            newYPos: Int = 0,
            newWidth: Int = 0,
            newHeight: Int = 0
        ): IntRectangle
    }
}
