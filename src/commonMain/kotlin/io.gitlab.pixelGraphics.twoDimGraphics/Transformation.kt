package io.gitlab.pixelGraphics.twoDimGraphics

/**
 * Is a two-dimensional affine transformation that maps all coordinates, and other drawing instruments from the user
 * space into the surface's canonical coordinate system, also known as the device space.
 */
public expect class Transformation(canvas: Canvas) {
    /**
     * Modifies the current transformation matrix (CTM) by translating the user-space origin by (xPos, yPos). This
     * offset is interpreted as a user-space coordinate according to the CTM in place **BEFORE** the new call to
     * [translate]. In other words the translation of the user-space origin takes place after any existing
     * transformation.
     * @param xPos The amount to translate in the X direction.
     * @param yPos The amount to translate in the Y direction.
     */
    public fun translate(xPos: Double, yPos: Double)

    /**
     * Modifies the current transformation matrix (CTM) by scaling the X and Y user-space axes by [xPos] and [yPos]
     * respectively. The scaling of the axes takes place after any existing transformation of user space.
     * @param xPos Scale factor for the X dimension.
     * @param yPos Scale factor for the Y dimension.
     */
    public fun scale(xPos: Double, yPos: Double)

    /**
     * Modifies the current transformation matrix (CTM) by rotating the user-space axes by angle radians. The rotation
     * of the axes takes places after any existing transformation of user space. The rotation direction for positive
     * angles is from the positive X axis toward the positive Y axis.
     * @param angle The angle (in radians) by which the user-space axes will be rotated.
     */
    public fun rotate(angle: Double)

    /**
     * Transform a coordinate from user space to device space by multiplying the given point by the current
     * transformation matrix (CTM).
     * @param userXPos X value of coordinate in user space.
     * @param userYPos Y value of coordinate in user space.
     * @return A Pair containing the following:
     * 1. devXPos - Device space X coordinate.
     * 2. devYPos - Device space Y coordinate.
     */
    public fun userToDeviceCoordinates(userXPos: Double, userYPos: Double): Pair<Double, Double>

    /**
     * Transform a distance vector from user space to device space. This function is similar to
     * [userToDeviceCoordinates] except that the translation components of the CTM will be ignored when transforming
     * (userXPos, userYPos).
     * @param userXPos X value of distance vector in user space.
     * @param userYPos Y value of distance vector in user space.
     * @return A Pair containing the following:
     * 1. devXPos - Device space X value of distance vector.
     * 2. devYPos - Device space Y value of distance vector.
     */
    public fun userToDeviceDistance(userXPos: Double, userYPos: Double): Pair<Double, Double>

    /**
     * Transform a coordinate from device space to user space by multiplying the given point by the current
     * transformation matrix (CTM).
     * @param devXPos X value of coordinate in device space.
     * @param devYPos Y value of coordinate in device space.
     * @return A Pair containing the following:
     * 1. userXPos - User space X coordinate.
     * 2. userYPos - User space Y coordinate.
     */
    public fun deviceToUserCoordinates(devXPos: Double, devYPos: Double): Pair<Double, Double>

    /**
     * Transform a distance vector from device space to user space. This function is similar to
     * [deviceToUserCoordinates] except that the translation components of the CTM will be ignored when transforming
     * (devXPos, devYPos).
     * @param devXPos X value of distance vector in device space.
     * @param devYPos Y value of distance vector in device space.
     * @return A Pair containing the following:
     * 1. userXPos - User space X value of distance vector.
     * 2. userYPos - User space Y value of distance vector.
     */
    public fun deviceToUserDistance(devXPos: Double, devYPos: Double): Pair<Double, Double>
}
