package io.gitlab.pixelGraphics.twoDimGraphics.text

import io.gitlab.pixelGraphics.twoDimGraphics.style.AntiAliasType
import io.gitlab.pixelGraphics.twoDimGraphics.Closable
import io.gitlab.pixelGraphics.twoDimGraphics.SubPixelOrder

/**
 * Specifies how a font should be rendered.
 */
public expect class FontOptions : Closable {
    /** Whether an error has previously occurred for this [FontOptions] object. */
    public val status: UInt
    /** The antialiasing mode for this [FontOptions] object. */
    public var antiAliasType: AntiAliasType
    /**
     * The subpixel order for this [FontOptions] object. The subpixel order specifies the order of color elements
     * within each pixel on the display device when rendering with an anti aliasing mode of [AntiAliasType.SUBPIXEL].
     * @see SubPixelOrder
     */
    public var subPixelOrder: SubPixelOrder
    /**
     * The hint style for font outlines for the font options object. This controls whether to fit font outlines to the
     * pixel grid, and if so whether to optimize for fidelity or contrast.
     * @see FontHintStyle
     */
    public var hintStyle: FontHintStyle
    /**
     * The metrics hinting mode for the font options object. This controls whether metrics are quantized to integer
     * values in device units.
     * @see FontHintMetrics
     */
    public var hintMetrics: FontHintMetrics

    public companion object {
        /**
         * Creates a new instance of [FontOptions] with all font options initialized to default values.
         * @return An instance of [FontOptions].
         */
        public fun create(): FontOptions
    }

    /**
     * Merges non-default options from [other] into this [FontOptions] object, replacing existing values. This
     * operation can be thought of as somewhat similar to compositing other onto options with the operation of
     * `CAIRO_OPERATOR_OVER`.
     */
    public fun merge(other: FontOptions)
}
