package io.gitlab.pixelGraphics.twoDimGraphics.text

import io.gitlab.pixelGraphics.twoDimGraphics.Canvas

/** Renders text on a [Canvas]. */
public expect class Text(canvas: Canvas) {
    /**
     * A set of custom font rendering options for the [Canvas]. Rendering options are derived by merging these options
     * with the options derived from underlying surface. If the value in has a default value
     * (like [io.gitlab.pixelGraphics.twoDimGraphics.AntiAliasType.DEFAULT]) then the value from the surface is used.
     */
    public var fontOptions: FontOptions

    /**
     * Selects a family and style of font from a simplified description as a [family name][fontFamily], [slant] and
     * [weight]. There is no operation to list available family names on the system, but the standard CSS2 generic
     * family names, (**serif**, **sans-serif**, **cursive**, **fantasy**, **monospace**), are likely to work as
     * expected. If [fontFamily] starts with the string **cairo :**, or if no native font backends are compiled in then
     * Cairo will use an internal font family. The internal font family recognizes many modifiers in the family string,
     * most notably it recognizes the string **monospace**. That is, the family name **cairo :monospace** will use the
     * monospace version of the internal font family.
     *
     * For **real** font selection, see the font-backend-specific `font_face_create` functions for the font backend
     * you are using. (For example, if you are using the freetype-based cairo-ft font backend, see
     * `cairo_ft_font_face_create_for_ft_face` or `cairo_ft_font_face_create_for_pattern`) The resulting font face
     * could then be used with `cairo_scaled_font_create`, and `cairo_set_scaled_font`. Similarly when using the
     * **real** font support you can call directly into the underlying font system (such as fontconfig or freetype)
     * for operations such as listing available fonts, etc.
     *
     * It is expected that most applications will need to use a more comprehensive font handling, and text layout
     * library (for example, Pango), in conjunction to this library. If text is drawn without a call to
     * [selectFontFace] (nor [selectFontFace] nor `cairo_set_scaled_font`) then the default family is
     * platform-specific, but is essentially **sans-serif**. Default slant is [FontSlant.NORMAL], and default weight is
     * [FontWeight.NORMAL].
     * @param fontFamily The font family name.
     * @param slant The slant for the font.
     * @param weight The weight for the font.
     */
    public fun selectFontFace(
        fontFamily: String,
        slant: FontSlant = FontSlant.NORMAL,
        weight: FontWeight = FontWeight.NORMAL
    )

    /**
     * Sets the current font matrix to a scale by a factor of [size], replacing any font matrix previously set with
     * [changeFontSize] or `cairo_set_font_matrix`. This results in a font size of size user space units. More
     * precisely this matrix will result in the font's em-square being a size by size square in user space. If text is
     * drawn without a call to [changeFontSize] (nor `cairo_set_font_matrix` nor `cairo_set_scaled_font`) then the
     * default font size is *10.0*.
     * @param size The new font size in user space units.
     */
    public fun changeFontSize(size: Double)

    /**
     * A drawing operator that generates the shape from a string of UTF-8 characters, rendered according to the current
     * font_face, font_size (font_matrix), and font_options. This function first computes a set of glyphs for the
     * string of text. The first glyph is placed so that its origin is at the current point. The origin of each
     * subsequent glyph is offset from that of the previous glyph by the advance values of the previous glyph.
     *
     * After this call the current point is moved to the origin of where the next glyph would be placed in this same
     * progression. That is the current point will be at the origin of the final glyph offset by its advance values.
     * This allows for easy display of a single logical string with multiple calls to [drawText].
     * @param txt The text (in UTF-8 format) to draw.
     */
    public fun drawText(txt: String)
}
