package io.gitlab.pixelGraphics.twoDimGraphics.text

/** Variants of a font face based on their weight. */
public enum class FontWeight {
    /** Normal font weight. */
    NORMAL,
    /** Bold font weight. */
    BOLD,
    /** Unrecognized font weight. */
    UNKNOWN
}
