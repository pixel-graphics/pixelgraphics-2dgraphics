package io.gitlab.pixelGraphics.twoDimGraphics.text

/**
 * Specifies whether to hint font metrics; hinting font metrics means quantizing them so that they are integer values
 * in device space. Doing this improves the consistency of letter and line spacing, however it also means that text
 * will be laid out differently at different zoom factors.
 */
public enum class FontHintMetrics {
    /** Hint metrics in the default manner for the font backend and target device */
    DEFAULT,
    /** Do not hint font metrics. */
    OFF,
    /** Use font hint metrics. */
    ON,
    /** Unrecognized font hint metrics. */
    UNKNOWN
}
