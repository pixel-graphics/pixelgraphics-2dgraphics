package io.gitlab.pixelGraphics.twoDimGraphics.text

/** Variants of a font face based on their slant. */
public enum class FontSlant {
    /** Upright font style. */
    NORMAL,
    /** Italic font style. */
    ITALIC,
    /** Oblique font style. */
    OBLIQUE,
    /** Unrecognized font style. */
    UNKNOWN
}
