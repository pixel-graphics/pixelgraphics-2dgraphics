package io.gitlab.pixelGraphics.twoDimGraphics.surface

import cairo.*
import kotlinx.cinterop.*

public actual class PdfSurface private constructor(
    public override val surfacePtr: CPointer<cairo_surface_t>?
) : Surface {
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false

    override fun close() {
        if (!isClosed) cairo_surface_destroy(surfacePtr)
        _isClosed = true
    }

    public actual companion object {
        public actual fun create(filePath: String, width: Double, height: Double): PdfSurface {
            val ptr = cairo_pdf_surface_create(filename = filePath, width_in_points = width,
                height_in_points = height)
            val errorMsg = "Failed to create PdfSurface."
            return if (ptr != null) PdfSurface(ptr) else throw IllegalStateException(errorMsg)
        }
    }

    /**
     * Restricts the generated PDF file to [version]. This function should only be called **before** any drawing
     * operations have been performed on the given surface. The simplest way to do this is to call this function
     * immediately **after** creating the surface.
     * @param version The PDF version to use.
     */
    public fun restrictToVersion(version: _cairo_pdf_version) {
        if (isClosed) throw IllegalStateException("PdfSurface is closed.")
        cairo_pdf_surface_restrict_to_version(surfacePtr, version)
    }

    public actual fun changeSize(width: Double, height: Double) {
        if (isClosed) throw IllegalStateException("PdfSurface is closed.")
        cairo_pdf_surface_set_size(surface = surfacePtr, width_in_points = width, height_in_points = height)
    }
}
