package io.gitlab.pixelGraphics.twoDimGraphics

import cairo.cairo_rectangle_int_t
import cairo.cairo_rectangle_t
import kotlinx.cinterop.Arena
import kotlinx.cinterop.alloc

public actual class IntRectangle private constructor() : Closable {
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false
    private val arena = Arena()
    public val cairoRect: cairo_rectangle_int_t = arena.alloc()
    public actual var xPos: Int
        get() = if (isClosed) throw IllegalStateException("IntRectangle is closed.") else cairoRect.x
        set(value) {
            if (isClosed) throw IllegalStateException("IntRectangle is closed.")
            cairoRect.x = value
        }
    public actual var yPos: Int
        get() = if (isClosed) throw IllegalStateException("IntRectangle is closed.") else cairoRect.y
        set(value) {
            if (isClosed) throw IllegalStateException("IntRectangle is closed.")
            cairoRect.y = value
        }
    public actual var width: Int
        get() = if (isClosed) throw IllegalStateException("IntRectangle is closed.") else cairoRect.width
        set(value) {
            if (isClosed) throw IllegalStateException("IntRectangle is closed.")
            cairoRect.width = value
        }
    public actual var height: Int
        get() = if (isClosed) throw IllegalStateException("IntRectangle is closed.") else cairoRect.height
        set(value) {
            if (isClosed) throw IllegalStateException("IntRectangle is closed.")
            cairoRect.height = value
        }

    public actual companion object {
        public fun fromCairoIntRectangle(cairoRect: cairo_rectangle_int_t): IntRectangle = IntRectangle().apply {
            xPos = cairoRect.x
            yPos = cairoRect.y
            width = cairoRect.width
            height = cairoRect.height
        }

        public actual fun create(
            newXPos: Int,
            newYPos: Int,
            newWidth: Int,
            newHeight: Int
        ): IntRectangle = IntRectangle().apply {
            xPos = newXPos
            yPos = newYPos
            width = newWidth
            height = newHeight
        }
    }

    override fun close() {
        if (!isClosed) {
            _isClosed = true
            arena.clear()
        }
    }
}
