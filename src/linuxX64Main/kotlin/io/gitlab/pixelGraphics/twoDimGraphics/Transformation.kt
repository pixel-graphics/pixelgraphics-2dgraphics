package io.gitlab.pixelGraphics.twoDimGraphics

import cairo.*
import kotlinx.cinterop.*

public actual class Transformation actual constructor(public val canvas: Canvas) {
    public actual fun translate(xPos: Double, yPos: Double) {
        cairo_translate(cr = canvas.cairoPtr, tx = xPos, ty = yPos)
    }

    public actual fun scale(xPos: Double, yPos: Double) {
        cairo_scale(cr = canvas.cairoPtr, sx = xPos, sy = yPos)
    }

    public actual fun rotate(angle: Double) {
        cairo_rotate(canvas.cairoPtr, angle)
    }

    public actual fun userToDeviceCoordinates(userXPos: Double, userYPos: Double): Pair<Double, Double> = memScoped {
        val tmpXPos = alloc<DoubleVar>()
        val tmpYPos = alloc<DoubleVar>()
        tmpXPos.value = userXPos
        tmpYPos.value = userYPos
        cairo_user_to_device(cr = canvas.cairoPtr, x = tmpXPos.ptr, y = tmpYPos.ptr)
        tmpXPos.value to tmpYPos.value
    }

    public actual fun userToDeviceDistance(userXPos: Double, userYPos: Double): Pair<Double, Double> = memScoped {
        val tmpXPos = alloc<DoubleVar>()
        val tmpYPos = alloc<DoubleVar>()
        tmpXPos.value = userXPos
        tmpYPos.value = userYPos
        cairo_user_to_device_distance(cr = canvas.cairoPtr, dx = tmpXPos.ptr, dy = tmpYPos.ptr)
        tmpXPos.value to tmpYPos.value
    }

    public actual fun deviceToUserCoordinates(devXPos: Double, devYPos: Double): Pair<Double, Double> = memScoped {
        val tmpXPos = alloc<DoubleVar>()
        val tmpYPos = alloc<DoubleVar>()
        tmpXPos.value = devXPos
        tmpYPos.value = devYPos
        cairo_device_to_user(cr = canvas.cairoPtr, x = tmpXPos.ptr, y = tmpYPos.ptr)
        tmpXPos.value to tmpYPos.value
    }

    public actual fun deviceToUserDistance(devXPos: Double, devYPos: Double): Pair<Double, Double> = memScoped {
        val tmpXPos = alloc<DoubleVar>()
        val tmpYPos = alloc<DoubleVar>()
        tmpXPos.value = devXPos
        tmpYPos.value = devYPos
        cairo_device_to_user_distance(cr = canvas.cairoPtr, dx = tmpXPos.ptr, dy = tmpYPos.ptr)
        tmpXPos.value to tmpYPos.value
    }
}
