package io.gitlab.pixelGraphics.twoDimGraphics.text

import cairo._cairo_hint_style

internal fun _cairo_hint_style.toFontHintStyle(): FontHintStyle = when (this) {
    _cairo_hint_style.CAIRO_HINT_STYLE_DEFAULT -> FontHintStyle.DEFAULT
    _cairo_hint_style.CAIRO_HINT_STYLE_NONE -> FontHintStyle.NONE
    _cairo_hint_style.CAIRO_HINT_STYLE_SLIGHT -> FontHintStyle.SLIGHT
    _cairo_hint_style.CAIRO_HINT_STYLE_MEDIUM -> FontHintStyle.MEDIUM
    _cairo_hint_style.CAIRO_HINT_STYLE_FULL -> FontHintStyle.FULL
    else -> FontHintStyle.UNKNOWN
}

internal fun FontHintStyle.toCairoHintStyle(): _cairo_hint_style = when (this) {
    FontHintStyle.DEFAULT -> _cairo_hint_style.CAIRO_HINT_STYLE_DEFAULT
    FontHintStyle.NONE -> _cairo_hint_style.CAIRO_HINT_STYLE_NONE
    FontHintStyle.SLIGHT -> _cairo_hint_style.CAIRO_HINT_STYLE_SLIGHT
    FontHintStyle.MEDIUM -> _cairo_hint_style.CAIRO_HINT_STYLE_MEDIUM
    FontHintStyle.FULL -> _cairo_hint_style.CAIRO_HINT_STYLE_FULL
    FontHintStyle.UNKNOWN -> _cairo_hint_style.CAIRO_HINT_STYLE_DEFAULT
}
