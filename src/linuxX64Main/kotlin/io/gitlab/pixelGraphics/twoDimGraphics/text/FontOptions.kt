package io.gitlab.pixelGraphics.twoDimGraphics.text

import cairo.*
import io.gitlab.pixelGraphics.twoDimGraphics.*
import io.gitlab.pixelGraphics.twoDimGraphics.style.AntiAliasType
import io.gitlab.pixelGraphics.twoDimGraphics.style.toAntiAliasType
import io.gitlab.pixelGraphics.twoDimGraphics.style.toCairoAntiAlias
import kotlinx.cinterop.CPointer

public actual class FontOptions private constructor(ptr: CPointer<cairo_font_options_t>?): Closable {
    public val fontOptionsPtr: CPointer<cairo_font_options_t>? = ptr
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false
    public actual val status: UInt
        get() = if (isClosed) {
            throw IllegalStateException("FontOptions is closed.")
        } else {
            cairo_font_options_status(fontOptionsPtr)
        }
    public actual var antiAliasType: AntiAliasType
        get() = if (isClosed) {
            throw IllegalStateException("FontOptions is closed.")
        } else {
            cairo_font_options_get_antialias(fontOptionsPtr).toAntiAliasType()
        }
        set(value) {
            if (isClosed) throw IllegalStateException("FontOptions is closed.")
            cairo_font_options_set_antialias(fontOptionsPtr, value.toCairoAntiAlias())
        }
    public actual var subPixelOrder: SubPixelOrder
        get() = if (isClosed) {
            throw IllegalStateException("FontOptions is closed.")
        } else {
            cairo_font_options_get_subpixel_order(fontOptionsPtr).toSubPixelOrder()
        }
        set(value) {
            if (isClosed) throw IllegalStateException("FontOptions is closed.")
            cairo_font_options_set_subpixel_order(fontOptionsPtr, value.toCairoSubPixelOrder())
        }
    public actual var hintStyle: FontHintStyle
        get() = if (isClosed) {
            throw IllegalStateException("FontOptions is closed.")
        } else {
            cairo_font_options_get_hint_style(fontOptionsPtr).toFontHintStyle()
        }
        set(value) {
            if (isClosed) throw IllegalStateException("FontOptions is closed.")
            cairo_font_options_set_hint_style(fontOptionsPtr, value.toCairoHintStyle())
        }
    public actual var hintMetrics: FontHintMetrics
        get() = if (isClosed) {
            throw IllegalStateException("FontOptions is closed.")
        } else {
            cairo_font_options_get_hint_metrics(fontOptionsPtr).toFontHintMetrics()
        }
        set(value) {
            if (isClosed) throw IllegalStateException("FontOptions is closed.")
            cairo_font_options_set_hint_metrics(fontOptionsPtr, value.toCairoHintMetrics())
        }

    public actual companion object {
        public actual fun create(): FontOptions = FontOptions(cairo_font_options_create())

        public fun fromPointer(ptr: CPointer<cairo_font_options_t>?): FontOptions = FontOptions(ptr)
    }

    public actual fun merge(other: FontOptions) {
        if (isClosed) throw IllegalStateException("FontOptions is closed.")
        cairo_font_options_merge(fontOptionsPtr, other.fontOptionsPtr)
    }

    override fun close() {
        if (!isClosed) cairo_font_options_destroy(fontOptionsPtr)
        _isClosed = true
    }
}
