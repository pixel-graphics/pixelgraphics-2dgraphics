package io.gitlab.pixelGraphics.twoDimGraphics

import cairo.*
import io.gitlab.pixelGraphics.twoDimGraphics.style.*
import io.gitlab.pixelGraphics.twoDimGraphics.surface.Surface
import kotlinx.cinterop.*

public actual class Canvas private constructor(
    ptr: CPointer<cairo_t>? = null,
    public val surface: Surface? = null
) : Closable {
    public val cairoPtr: CPointer<cairo_t>? = if (surface != null) cairo_create(surface.surfacePtr) else ptr
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false
    public actual val dashCount: Int
        get() = if (isClosed) throw IllegalStateException("Canvas is closed.") else cairo_get_dash_count(cairoPtr)
    public actual var lineJoin: LineJoin
        get() = if (isClosed) {
            throw IllegalStateException("Canvas is closed.")
        } else {
            cairo_get_line_join(cairoPtr).toLineJoin()
        }
        set(value) {
            if (isClosed) throw IllegalStateException("Canvas is closed.")
            cairo_set_line_join(cairoPtr, value.toCairoLineJoin())
        }
    public actual var lineCap: LineCap
        get() = if (isClosed) {
            throw IllegalStateException("Canvas is closed.")
        } else {
            cairo_get_line_cap(cairoPtr).toLineCap()
        }
        set(value) {
            if (isClosed) throw IllegalStateException("Canvas is closed.")
            cairo_set_line_cap(cairoPtr, value.toCairoLineCap())
        }
    public actual var fillRule: FillRule
        get() = if (isClosed) {
            throw IllegalStateException("Canvas is closed.")
        } else {
            cairo_get_fill_rule(cairoPtr).toFillRule()
        }
        set(value) {
            if (isClosed) throw IllegalStateException("Canvas is closed.")
            cairo_set_fill_rule(cairoPtr, value.toCairoFillRule())
        }
    public actual var lineWidth: Double
        get() = if (isClosed) throw IllegalStateException("Canvas is closed.") else cairo_get_line_width(cairoPtr)
        set(value) {
            if (isClosed) throw IllegalStateException("Canvas is closed.")
            cairo_set_line_width(cairoPtr, value)
        }
    public actual var miterLimit: Double
        get() = if (isClosed) throw IllegalStateException("Canvas is closed.") else cairo_get_miter_limit(cairoPtr)
        set(value) {
            if (isClosed) throw IllegalStateException("Canvas is closed.")
            cairo_set_miter_limit(cairoPtr, value)
        }

    /**
     * The compositing operator to be used for **ALL** drawing operations. See [cairo_operator_t] for details on the
     * semantics of each available compositing operator. The default operator is `CAIRO_OPERATOR_OVER`.
     */
    public var operator: _cairo_operator
        get() = if (isClosed) throw IllegalStateException("Canvas is closed.") else cairo_get_operator(cairoPtr)
        set(value) {
            if (isClosed) throw IllegalStateException("Canvas is closed.")
            cairo_set_operator(cairoPtr, value)
        }
    public actual var tolerance: Double
        get() = if (isClosed) throw IllegalStateException("Canvas is closed.") else cairo_get_tolerance(cairoPtr)
        set(value) {
            if (isClosed) throw IllegalStateException("Canvas is closed.")
            cairo_set_tolerance(cairoPtr, value)
        }

    public actual fun use(init: Canvas.() -> Unit) {
        this.init()
        surface?.close()
        close()
    }

    override fun close() {
        if (!isClosed) cairo_destroy(cairoPtr)
        _isClosed = true
    }

    public actual companion object {
        public actual fun create(surface: Surface): Canvas = Canvas(surface = surface)

        public fun fromPointer(ptr: CPointer<cairo_t>?): Canvas = Canvas(ptr = ptr)
    }

    public actual fun changeSourceRgb(red: Double, green: Double, blue: Double) {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        cairo_set_source_rgb(cr = cairoPtr, red = red, green = green, blue = blue)
    }

    public actual fun statusCode(): UInt = if (isClosed) {
        throw IllegalStateException("Canvas is closed.")
    } else {
        cairo_status(cairoPtr)
    }

    public actual fun changeSourceRgba(red: Double, green: Double, blue: Double, alpha: Double) {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        cairo_set_source_rgba(cr = cairoPtr, red = red, green = green, blue = blue, alpha = alpha)
    }

    public actual fun changeDash(dashes: DoubleArray, offset: Double) {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        dashes.usePinned { pinned ->
            cairo_set_dash(cr = cairoPtr, dashes = pinned.addressOf(0), offset = offset, num_dashes = dashes.size)
        }
    }

    public actual fun fetchDash(): Pair<DoubleArray, Double> = memScoped {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        val offset = alloc<DoubleVar>()
        val dashes = DoubleArray(dashCount)
        dashes.usePinned { pinned ->
            cairo_get_dash(cr = cairoPtr, offset = offset.ptr, dashes = pinned.addressOf(0))
        }
        dashes to offset.value
    }

    public actual fun fill() {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        cairo_fill(cairoPtr)
    }

    public actual fun fillPreserve() {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        cairo_fill_preserve(cairoPtr)
    }

    public actual fun fillExtents(): FillExtentsResult = memScoped {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        val x1 = alloc<DoubleVar>()
        val x2 = alloc<DoubleVar>()
        val y1 = alloc<DoubleVar>()
        val y2 = alloc<DoubleVar>()
        cairo_fill_extents(cr = cairoPtr, x1 = x1.ptr, x2 = x2.ptr, y1 = y1.ptr, y2 = y2.ptr)
        return FillExtentsResult(x1 = x1.value, x2 = x2.value, y1 = y1.value, y2 = y2.value)
    }

    public actual fun inFill(xPos: Double, yPos: Double): Boolean = if (isClosed) {
        throw IllegalStateException("Canvas is closed.")
    } else {
        cairo_in_fill(cr = cairoPtr, x = xPos, y = yPos) == 1
    }

    public actual fun stroke() {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        cairo_stroke(cairoPtr)
    }

    public actual fun strokePreserve() {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        cairo_stroke_preserve(cairoPtr)
    }

    public actual fun strokeExtents(): StrokeExtentsResult = memScoped {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        val x1 = alloc<DoubleVar>()
        val x2 = alloc<DoubleVar>()
        val y1 = alloc<DoubleVar>()
        val y2 = alloc<DoubleVar>()
        cairo_stroke_extents(cr = cairoPtr, x1 = x1.ptr, x2 = x2.ptr, y1 = y1.ptr, y2 = y2.ptr)
        return StrokeExtentsResult(x1 = x1.value, x2 = x2.value, y1 = y1.value, y2 = y2.value)
    }

    public actual fun inStroke(xPos: Double, yPos: Double): Boolean = if (isClosed) {
        throw IllegalStateException("Canvas is closed.")
    } else {
        cairo_in_stroke(cr = cairoPtr, x = xPos, y = yPos) == 1
    }

    public actual fun copyPage() {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        cairo_copy_page(cairoPtr)
    }

    public actual fun showPage() {
        if (isClosed) throw IllegalStateException("Canvas is closed.")
        cairo_show_page(cairoPtr)
    }

    public actual fun changeSourceSurface(surface: Surface, xPos: Double, yPos: Double) {
        cairo_set_source_surface(cr = cairoPtr, surface = surface.surfacePtr, x = xPos, y = yPos)
    }
}

public fun canvas(ptr: CPointer<cairo_t>? = null, surface: Surface?, init: Canvas.() -> Unit): Canvas {
    val errorMsg = "A non null ptr or surface argument must be provided."
    if (ptr == null && surface == null) throw IllegalArgumentException(errorMsg)
    val result = if (surface != null) Canvas.create(surface) else Canvas.fromPointer(ptr)
    result.init()
    return result
}
