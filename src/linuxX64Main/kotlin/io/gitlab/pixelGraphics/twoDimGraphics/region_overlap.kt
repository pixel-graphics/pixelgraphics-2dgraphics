package io.gitlab.pixelGraphics.twoDimGraphics

import cairo._cairo_region_overlap

internal fun RegionOverlap.toCairoRegionOverlap(): _cairo_region_overlap = when (this) {
    RegionOverlap.IN -> _cairo_region_overlap.CAIRO_REGION_OVERLAP_IN
    RegionOverlap.OUT -> _cairo_region_overlap.CAIRO_REGION_OVERLAP_OUT
    RegionOverlap.PART -> _cairo_region_overlap.CAIRO_REGION_OVERLAP_PART
    RegionOverlap.UNKNOWN -> _cairo_region_overlap.CAIRO_REGION_OVERLAP_IN
}

internal fun _cairo_region_overlap.toRegionOverlap(): RegionOverlap = when (this) {
    _cairo_region_overlap.CAIRO_REGION_OVERLAP_IN -> RegionOverlap.IN
    _cairo_region_overlap.CAIRO_REGION_OVERLAP_OUT -> RegionOverlap.OUT
    _cairo_region_overlap.CAIRO_REGION_OVERLAP_PART -> RegionOverlap.PART
    else -> RegionOverlap.UNKNOWN
}
