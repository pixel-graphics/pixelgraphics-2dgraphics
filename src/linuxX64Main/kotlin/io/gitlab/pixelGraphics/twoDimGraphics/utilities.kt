package io.gitlab.pixelGraphics.twoDimGraphics

import cairo.*
import io.gitlab.pixelGraphics.twoDimGraphics.surface.Surface
import kotlinx.cinterop.*

public actual fun statusCodeToString(statusCode: UInt): String = cairo_status_to_string(statusCode)?.toKString() ?: ""

public actual fun surfaceStatusCode(surface: Surface): UInt = cairo_surface_status(surface.surfacePtr)

/**
 * Used to retrieve the list of supported PDF versions.
 * @return An Array of supported PDF versions.
 */
public fun fetchSupportedPdfVersions(): Array<_cairo_pdf_version> = memScoped {
    val versions = alloc<CPointerVar<cairo_pdf_version_tVar>>()
    val length = alloc<IntVar>()
    val tmpList = mutableListOf<_cairo_pdf_version>()
    cairo_pdf_get_versions(versions.ptr, length.ptr)
    var pos = 0
    while (pos < length.value) {
        val tmpItem = versions.value?.get(pos)?.value
        if (tmpItem != null) tmpList += tmpItem
        pos++
    }
    return tmpList.toTypedArray()
}

/**
 * Used to retrieve the list of supported SVG versions.
 * @return An Array of supported SVG versions.
 */
public fun fetchSupportedSvgVersions(): Array<_cairo_svg_version> = memScoped {
    val versions = alloc<CPointerVar<cairo_svg_version_tVar>>()
    val length = alloc<IntVar>()
    val tmpList = mutableListOf<_cairo_svg_version>()
    cairo_svg_get_versions(versions.ptr, length.ptr)
    var pos = 0
    while (pos < length.value) {
        val tmpItem = versions.value?.get(pos)?.value
        if (tmpItem != null) tmpList += tmpItem
        pos++
    }
    return tmpList.toTypedArray()
}

/**
 * Gets the [String] representation of the given PDF [version ID][version]. This function will return *""* (an empty
 * [String]) if [version] isn't valid.
 * @param version A version ID.
 * @return The [String] associated with the given [version].
 */
public fun pdfVersionToString(version: _cairo_pdf_version): String =
    cairo_pdf_version_to_string(version)?.toKString() ?: ""

/**
 * Gets the [String] representation of the given SVG [version ID][version]. This function will return *""* (an empty
 * [String]) if [version] isn't valid.
 * @param version A version ID.
 * @return The [String] associated with the given [version].
 */
public fun svgVersionToString(version: _cairo_svg_version): String =
    cairo_svg_version_to_string(version)?.toKString() ?: ""

