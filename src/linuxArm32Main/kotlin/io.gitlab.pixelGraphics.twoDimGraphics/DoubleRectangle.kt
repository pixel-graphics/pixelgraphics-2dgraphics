package io.gitlab.pixelGraphics.twoDimGraphics

import cairo.cairo_rectangle_t
import kotlinx.cinterop.Arena
import kotlinx.cinterop.alloc

public actual class DoubleRectangle private constructor() : Closable {
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false
    private val arena = Arena()
    public val cairoRect: cairo_rectangle_t = arena.alloc()
    public actual var xPos: Double
        get() = if (isClosed) throw IllegalStateException("DoubleRectangle is closed.") else cairoRect.x
        set(value) {
            if (isClosed) throw IllegalStateException("DoubleRectangle is closed.")
            cairoRect.x = value
        }
    public actual var yPos: Double
        get() = if (isClosed) throw IllegalStateException("DoubleRectangle is closed.") else cairoRect.y
        set(value) {
            if (isClosed) throw IllegalStateException("DoubleRectangle is closed.")
            cairoRect.y = value
        }
    public actual var width: Double
        get() = if (isClosed) throw IllegalStateException("DoubleRectangle is closed.") else cairoRect.width
        set(value) {
            if (isClosed) throw IllegalStateException("DoubleRectangle is closed.")
            cairoRect.width = value
        }
    public actual var height: Double
        get() = if (isClosed) throw IllegalStateException("DoubleRectangle is closed.") else cairoRect.height
        set(value) {
            if (isClosed) throw IllegalStateException("DoubleRectangle is closed.")
            cairoRect.height = value
        }

    public actual companion object {
        public fun fromCairoRectangle(cairoRect: cairo_rectangle_t): DoubleRectangle = DoubleRectangle().apply {
            xPos = cairoRect.x
            yPos = cairoRect.y
            width = cairoRect.width
            height = cairoRect.height
        }

        public actual fun create(
            newXPos: Double,
            newYPos: Double,
            newWidth: Double,
            newHeight: Double
        ): DoubleRectangle = DoubleRectangle().apply {
            xPos = newXPos
            yPos = newYPos
            width = newWidth
            height = newHeight
        }
    }

    override fun close() {
        if (!isClosed) {
            _isClosed = true
            arena.clear()
        }
    }
}
