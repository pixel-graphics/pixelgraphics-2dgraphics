package io.gitlab.pixelGraphics.twoDimGraphics

import cairo.*
import kotlinx.cinterop.*

public actual class Region private constructor(ptr: CPointer<cairo_region_t>?): Closable {
    public val regionPtr: CPointer<cairo_region_t>? = ptr
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false
    public actual val isEmpty: Boolean
        get() = cairo_region_is_empty(regionPtr) == 1
    public actual val totalRectangles: Int
        get() = cairo_region_num_rectangles(regionPtr)
    public actual val extents: IntRectangle
        get() {
            val result = IntRectangle.create()
            cairo_region_get_extents(regionPtr, result.cairoRect.ptr)
            return result
        }

    public actual fun containsPoint(xPos: Int, yPos: Int): Boolean =
        cairo_region_contains_point(region = regionPtr, x = xPos, y = yPos) == 1

    public actual fun fetchRectangle(num: Int): IntRectangle {
        val result = IntRectangle.create()
        cairo_region_get_rectangle(region = regionPtr, nth = num, rectangle = result.cairoRect.ptr)
        return result
    }

    public actual fun statusCode(): UInt = cairo_region_status(regionPtr)

    public actual fun containsRectangle(rect: IntRectangle): RegionOverlap =
        cairo_region_contains_rectangle(regionPtr, rect.cairoRect.ptr).toRegionOverlap()

    public actual fun translate(xPos: Int, yPos: Int) {
        cairo_region_translate(region = regionPtr, dx = xPos, dy = yPos)
    }

    public actual fun intersectRegion(other: Region): Pair<UInt, Region> =
        cairo_region_intersect(regionPtr, other.regionPtr) to this

    public actual fun intersectRectangle(rect: IntRectangle): Pair<UInt, Region> =
        cairo_region_intersect_rectangle(regionPtr, rect.cairoRect.ptr) to this

    public actual fun subtractRegion(other: Region): Pair<UInt, Region> =
        cairo_region_subtract(regionPtr, other.regionPtr) to this

    public actual fun subtractRectangle(rect: IntRectangle): Pair<UInt, Region> =
        cairo_region_subtract_rectangle(regionPtr, rect.cairoRect.ptr) to this

    public actual fun regionUnion(other: Region): Pair<UInt, Region> =
        cairo_region_union(regionPtr, other.regionPtr) to this

    public actual fun rectangleUnion(rect: IntRectangle): Pair<UInt, Region> =
        cairo_region_union_rectangle(regionPtr, rect.cairoRect.ptr) to this

    public actual fun xorRegion(other: Region): Pair<UInt, Region> =
        cairo_region_xor(regionPtr, other.regionPtr) to this

    public actual fun xorRectangle(rect: IntRectangle): Pair<UInt, Region> =
        cairo_region_xor_rectangle(regionPtr, rect.cairoRect.ptr) to this

    public actual companion object {
        public actual fun create(): Region = Region(cairo_region_create())

        public actual fun createWithRectangle(rect: IntRectangle): Region =
            Region(cairo_region_create_rectangle(rect.cairoRect.ptr))

        public actual fun createWithMultipleRectangles(vararg rectangles: IntRectangle): Region = memScoped {
            val tmpArr = allocArray<_cairo_rectangle_int>(rectangles.size)
            rectangles.forEachIndexed { pos, item ->
                tmpArr[pos].height = item.height
                tmpArr[pos].width = item.width
                tmpArr[pos].x = item.xPos
                tmpArr[pos].y = item.yPos
            }
            return Region(cairo_region_create_rectangles(tmpArr, rectangles.size))
        }
    }

    override fun close() {
        if (!isClosed) {
            _isClosed = true
            cairo_region_destroy(regionPtr)
        }
    }
}
