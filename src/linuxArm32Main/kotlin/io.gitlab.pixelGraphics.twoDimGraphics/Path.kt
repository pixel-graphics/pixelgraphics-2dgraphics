package io.gitlab.pixelGraphics.twoDimGraphics

import cairo.*
import kotlinx.cinterop.*

public actual class Path actual constructor(public val canvas: Canvas) {
    public actual val hasCurrentPoint: Boolean
        get() = cairo_has_current_point(canvas.cairoPtr) == 1

    public actual fun fetchCurrentPoint(): Pair<Double, Double> = memScoped {
        val xPos = alloc<DoubleVar>()
        val yPos = alloc<DoubleVar>()
        cairo_get_current_point(cr = canvas.cairoPtr, x = xPos.ptr, y = yPos.ptr)
        xPos.value to yPos.value
    }

    public actual fun newPath() {
        cairo_new_path(canvas.cairoPtr)
    }

    public actual fun newSubPath() {
        cairo_new_sub_path(canvas.cairoPtr)
    }

    public actual fun closePath() {
        cairo_close_path(canvas.cairoPtr)
    }

    public actual fun arc(xCenterPos: Double, yCenterPos: Double, radius: Double, angle1: Double, angle2: Double) {
        cairo_arc(cr = canvas.cairoPtr, xc = xCenterPos, yc = yCenterPos, radius = radius, angle1 = angle1,
            angle2 = angle2)
    }

    public actual fun arcNegative(
        xCenterPos: Double,
        yCenterPos: Double,
        radius: Double,
        angle1: Double,
        angle2: Double
    ) {
        cairo_arc_negative(cr = canvas.cairoPtr, xc = xCenterPos, yc = yCenterPos, radius = radius, angle1 = angle1,
            angle2 = angle2)
    }

    public actual fun curveTo(x1: Double, y1: Double, y2: Double, x2: Double, x3: Double, y3: Double) {
        cairo_curve_to(cr = canvas.cairoPtr, x1 = x1, y1 = y1, x2 = x2, y2 = y2, x3 = x3, y3 = y3)
    }

    public actual fun lineTo(xPos: Double, yPos: Double) {
        cairo_line_to(cr = canvas.cairoPtr, x = xPos, y = yPos)
    }

    public actual fun moveTo(xPos: Double, yPos: Double) {
        cairo_move_to(cr = canvas.cairoPtr, x = xPos, y = yPos)
    }

    public actual fun rectangle(xPos: Double, yPos: Double, width: Double, height: Double) {
        cairo_rectangle(cr = canvas.cairoPtr, x = xPos, y = yPos, width = width, height = height)
    }
}
