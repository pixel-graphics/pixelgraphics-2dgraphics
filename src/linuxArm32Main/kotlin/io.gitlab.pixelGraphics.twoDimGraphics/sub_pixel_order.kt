package io.gitlab.pixelGraphics.twoDimGraphics

import cairo._cairo_subpixel_order

internal fun _cairo_subpixel_order.toSubPixelOrder(): SubPixelOrder = when (this) {
    _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_DEFAULT -> SubPixelOrder.DEFAULT
    _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_RGB -> SubPixelOrder.RGB
    _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_BGR -> SubPixelOrder.BGR
    _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_VRGB -> SubPixelOrder.VRGB
    else -> SubPixelOrder.UNKNOWN
}

internal fun SubPixelOrder.toCairoSubPixelOrder(): _cairo_subpixel_order = when (this) {
    SubPixelOrder.DEFAULT -> _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_DEFAULT
    SubPixelOrder.RGB -> _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_RGB
    SubPixelOrder.BGR -> _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_BGR
    SubPixelOrder.VRGB -> _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_VRGB
    SubPixelOrder.UNKNOWN -> _cairo_subpixel_order.CAIRO_SUBPIXEL_ORDER_DEFAULT
}
