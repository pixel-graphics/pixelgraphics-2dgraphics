package io.gitlab.pixelGraphics.twoDimGraphics.text

import cairo._cairo_font_slant

internal fun FontSlant.toCairoFontSlant(): _cairo_font_slant = when (this) {
    FontSlant.NORMAL -> _cairo_font_slant.CAIRO_FONT_SLANT_NORMAL
    FontSlant.ITALIC -> _cairo_font_slant.CAIRO_FONT_SLANT_ITALIC
    FontSlant.OBLIQUE -> _cairo_font_slant.CAIRO_FONT_SLANT_OBLIQUE
    FontSlant.UNKNOWN -> _cairo_font_slant.CAIRO_FONT_SLANT_NORMAL
}
