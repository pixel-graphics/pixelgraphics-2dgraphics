package io.gitlab.pixelGraphics.twoDimGraphics.text

import cairo._cairo_hint_metrics

internal fun _cairo_hint_metrics.toFontHintMetrics(): FontHintMetrics = when (this) {
    _cairo_hint_metrics.CAIRO_HINT_METRICS_DEFAULT -> FontHintMetrics.DEFAULT
    _cairo_hint_metrics.CAIRO_HINT_METRICS_OFF -> FontHintMetrics.OFF
    _cairo_hint_metrics.CAIRO_HINT_METRICS_ON -> FontHintMetrics.ON
    else -> FontHintMetrics.UNKNOWN
}

internal fun FontHintMetrics.toCairoHintMetrics(): _cairo_hint_metrics = when (this) {
    FontHintMetrics.DEFAULT -> _cairo_hint_metrics.CAIRO_HINT_METRICS_DEFAULT
    FontHintMetrics.OFF -> _cairo_hint_metrics.CAIRO_HINT_METRICS_OFF
    FontHintMetrics.ON -> _cairo_hint_metrics.CAIRO_HINT_METRICS_ON
    FontHintMetrics.UNKNOWN -> _cairo_hint_metrics.CAIRO_HINT_METRICS_DEFAULT
}
