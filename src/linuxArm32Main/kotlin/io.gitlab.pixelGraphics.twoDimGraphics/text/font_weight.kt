package io.gitlab.pixelGraphics.twoDimGraphics.text

import cairo._cairo_font_weight

internal fun FontWeight.toCairoFontWeight(): _cairo_font_weight = when (this) {
    FontWeight.NORMAL -> _cairo_font_weight.CAIRO_FONT_WEIGHT_NORMAL
    FontWeight.BOLD -> _cairo_font_weight.CAIRO_FONT_WEIGHT_BOLD
    FontWeight.UNKNOWN -> _cairo_font_weight.CAIRO_FONT_WEIGHT_NORMAL
}
