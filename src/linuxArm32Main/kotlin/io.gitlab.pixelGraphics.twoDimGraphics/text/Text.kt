package io.gitlab.pixelGraphics.twoDimGraphics.text

import cairo.*
import io.gitlab.pixelGraphics.twoDimGraphics.Canvas

public actual class Text actual constructor(public val canvas: Canvas) {
    public actual var fontOptions: FontOptions
        get() {
            val result = FontOptions.create()
            cairo_get_font_options(canvas.cairoPtr, result.fontOptionsPtr)
            return result
        }
        set(value) = cairo_set_font_options(canvas.cairoPtr, value.fontOptionsPtr)
    public actual fun selectFontFace(fontFamily: String, slant: FontSlant, weight: FontWeight) {
        cairo_select_font_face(
            cr = canvas.cairoPtr,
            family = fontFamily,
            slant = slant.toCairoFontSlant(),
            weight = weight.toCairoFontWeight()
        )
    }

    public actual fun changeFontSize(size: Double) {
        cairo_set_font_size(canvas.cairoPtr, size)
    }

    public actual fun drawText(txt: String) {
        cairo_show_text(canvas.cairoPtr, txt)
    }
}
