package io.gitlab.pixelGraphics.twoDimGraphics.style

import cairo._cairo_fill_rule
import io.gitlab.pixelGraphics.twoDimGraphics.style.FillRule

internal fun FillRule.toCairoFillRule(): _cairo_fill_rule = when (this) {
    FillRule.EVEN_ODD -> _cairo_fill_rule.CAIRO_FILL_RULE_EVEN_ODD
    FillRule.WINDING -> _cairo_fill_rule.CAIRO_FILL_RULE_WINDING
    FillRule.UNKNOWN -> _cairo_fill_rule.CAIRO_FILL_RULE_EVEN_ODD
}

internal fun _cairo_fill_rule.toFillRule(): FillRule = when (this) {
    _cairo_fill_rule.CAIRO_FILL_RULE_WINDING -> FillRule.WINDING
    _cairo_fill_rule.CAIRO_FILL_RULE_EVEN_ODD -> FillRule.EVEN_ODD
    else -> FillRule.UNKNOWN
}
