package io.gitlab.pixelGraphics.twoDimGraphics.style

import cairo._cairo_line_cap
import io.gitlab.pixelGraphics.twoDimGraphics.style.LineCap

internal fun LineCap.toCairoLineCap(): _cairo_line_cap = when (this) {
    LineCap.BUTT -> _cairo_line_cap.CAIRO_LINE_CAP_BUTT
    LineCap.ROUND -> _cairo_line_cap.CAIRO_LINE_CAP_ROUND
    LineCap.SQUARE -> _cairo_line_cap.CAIRO_LINE_CAP_SQUARE
    LineCap.UNKNOWN -> _cairo_line_cap.CAIRO_LINE_CAP_BUTT
}

internal fun _cairo_line_cap.toLineCap(): LineCap = when (this) {
    _cairo_line_cap.CAIRO_LINE_CAP_SQUARE -> LineCap.SQUARE
    _cairo_line_cap.CAIRO_LINE_CAP_ROUND -> LineCap.ROUND
    _cairo_line_cap.CAIRO_LINE_CAP_BUTT -> LineCap.BUTT
    else -> LineCap.UNKNOWN
}
