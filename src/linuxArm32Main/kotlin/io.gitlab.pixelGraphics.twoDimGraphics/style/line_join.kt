package io.gitlab.pixelGraphics.twoDimGraphics.style

import cairo._cairo_line_join
import io.gitlab.pixelGraphics.twoDimGraphics.style.LineJoin

internal fun LineJoin.toCairoLineJoin(): _cairo_line_join = when (this) {
    LineJoin.MITER -> _cairo_line_join.CAIRO_LINE_JOIN_MITER
    LineJoin.BEVEL -> _cairo_line_join.CAIRO_LINE_JOIN_BEVEL
    LineJoin.ROUND -> _cairo_line_join.CAIRO_LINE_JOIN_ROUND
    LineJoin.UNKNOWN -> _cairo_line_join.CAIRO_LINE_JOIN_MITER
}

internal fun _cairo_line_join.toLineJoin(): LineJoin = when (this) {
    _cairo_line_join.CAIRO_LINE_JOIN_ROUND -> LineJoin.ROUND
    _cairo_line_join.CAIRO_LINE_JOIN_BEVEL -> LineJoin.BEVEL
    _cairo_line_join.CAIRO_LINE_JOIN_MITER -> LineJoin.MITER
    else -> LineJoin.UNKNOWN
}
