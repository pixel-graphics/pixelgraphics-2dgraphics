package io.gitlab.pixelGraphics.twoDimGraphics.style

import cairo._cairo_antialias
import io.gitlab.pixelGraphics.twoDimGraphics.style.AntiAliasType

internal fun _cairo_antialias.toAntiAliasType(): AntiAliasType = when (this) {
    _cairo_antialias.CAIRO_ANTIALIAS_SUBPIXEL -> AntiAliasType.SUBPIXEL
    _cairo_antialias.CAIRO_ANTIALIAS_GRAY -> AntiAliasType.GRAY
    _cairo_antialias.CAIRO_ANTIALIAS_NONE -> AntiAliasType.NONE
    _cairo_antialias.CAIRO_ANTIALIAS_DEFAULT -> AntiAliasType.DEFAULT
    else -> AntiAliasType.UNKNOWN
}

internal fun AntiAliasType.toCairoAntiAlias(): _cairo_antialias = when (this) {
    AntiAliasType.SUBPIXEL -> _cairo_antialias.CAIRO_ANTIALIAS_SUBPIXEL
    AntiAliasType.GRAY -> _cairo_antialias.CAIRO_ANTIALIAS_GRAY
    AntiAliasType.NONE -> _cairo_antialias.CAIRO_ANTIALIAS_NONE
    AntiAliasType.DEFAULT -> _cairo_antialias.CAIRO_ANTIALIAS_DEFAULT
    AntiAliasType.UNKNOWN -> _cairo_antialias.CAIRO_ANTIALIAS_DEFAULT
}
