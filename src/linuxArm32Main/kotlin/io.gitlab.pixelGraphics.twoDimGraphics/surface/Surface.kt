package io.gitlab.pixelGraphics.twoDimGraphics.surface

import cairo.cairo_surface_t
import io.gitlab.pixelGraphics.twoDimGraphics.Closable
import kotlinx.cinterop.CPointer

public actual interface Surface : Closable {
    public val surfacePtr: CPointer<cairo_surface_t>?
}
