package io.gitlab.pixelGraphics.twoDimGraphics.surface

import cairo.CAIRO_CONTENT_ALPHA
import cairo.CAIRO_CONTENT_COLOR
import cairo.CAIRO_CONTENT_COLOR_ALPHA
import cairo.cairo_content_t

internal fun cairo_content_t.toSurfaceContent(): SurfaceContent = when (this) {
    CAIRO_CONTENT_COLOR -> SurfaceContent.COLOR
    CAIRO_CONTENT_ALPHA -> SurfaceContent.ALPHA
    CAIRO_CONTENT_COLOR_ALPHA -> SurfaceContent.COLOR_ALPHA
    else -> SurfaceContent.UNKNOWN
}

internal fun SurfaceContent.toCairoContent(): cairo_content_t = when (this) {
    SurfaceContent.COLOR_ALPHA -> CAIRO_CONTENT_COLOR_ALPHA
    SurfaceContent.ALPHA -> CAIRO_CONTENT_ALPHA
    SurfaceContent.COLOR -> CAIRO_CONTENT_COLOR
    SurfaceContent.UNKNOWN -> CAIRO_CONTENT_COLOR_ALPHA
}
