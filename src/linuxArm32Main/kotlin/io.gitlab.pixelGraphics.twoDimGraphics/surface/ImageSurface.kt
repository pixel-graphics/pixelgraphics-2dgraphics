package io.gitlab.pixelGraphics.twoDimGraphics.surface

import cairo.*
import io.gitlab.pixelGraphics.twoDimGraphics.surfaceStatusCode
import kotlinx.cinterop.CPointer

public actual class ImageSurface private constructor(
    public override val surfacePtr: CPointer<cairo_surface_t>?
) : Surface {
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false

    override fun close() {
        if (!isClosed) cairo_surface_destroy(surfacePtr)
        _isClosed = true
    }

    public actual companion object {
        public actual fun create(format: Int, width: Int, height: Int): ImageSurface {
            val ptr = cairo_image_surface_create(format = format, width = width, height = height)
            val errorMsg = "Failed to create ImageSurface."
            return if (ptr != null) ImageSurface(ptr) else throw IllegalStateException(errorMsg)
        }

        public actual fun fromPngFile(filePath: String): Pair<ImageSurface?, UInt> {
            val ptr = cairo_image_surface_create_from_png(filePath)
            val surface = if (ptr != null) ImageSurface(ptr) else null
            val status = if (surface != null) surfaceStatusCode(surface) else CAIRO_STATUS_SUCCESS
            return surface to status
        }
    }

    public actual fun writeToPngFile(filePath: String): UInt = if (isClosed) {
        throw IllegalStateException("ImageSurface is closed.")
    } else {
        cairo_surface_write_to_png(surfacePtr, filePath)
    }
}
