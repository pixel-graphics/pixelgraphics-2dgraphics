package io.gitlab.pixelGraphics.twoDimGraphics.surface

import cairo.cairo_recording_surface_create
import cairo.cairo_recording_surface_ink_extents
import cairo.cairo_surface_destroy
import cairo.cairo_surface_t
import io.gitlab.pixelGraphics.twoDimGraphics.DoubleRectangle
import kotlinx.cinterop.*

public actual class RecordingSurface private constructor(ptr: CPointer<cairo_surface_t>?): Surface {
    override val surfacePtr: CPointer<cairo_surface_t>? = ptr
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false

    override fun close() {
        if (!isClosed) {
            _isClosed = true
            cairo_surface_destroy(surfacePtr)
        }
    }

    public actual companion object {
        public actual fun create(surfaceContent: SurfaceContent, extents: DoubleRectangle?): RecordingSurface =
            RecordingSurface(cairo_recording_surface_create(surfaceContent.toCairoContent(), extents?.cairoRect?.ptr))
    }

    public actual fun inkExtents(): InkExtentsResult = memScoped {
        val xPos = alloc<DoubleVar>()
        val yPos = alloc<DoubleVar>()
        val width = alloc<DoubleVar>()
        val height = alloc<DoubleVar>()
        cairo_recording_surface_ink_extents(surface = surfacePtr, x0 = xPos.ptr, y0 = yPos.ptr, width = width.ptr,
            height = height.ptr)
        return InkExtentsResult(xPos = xPos.value, yPos = yPos.value, width = width.value, height = height.value)
    }
}
