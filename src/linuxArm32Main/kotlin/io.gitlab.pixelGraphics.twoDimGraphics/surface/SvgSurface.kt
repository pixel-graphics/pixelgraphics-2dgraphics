package io.gitlab.pixelGraphics.twoDimGraphics.surface

import cairo.*
import io.gitlab.pixelGraphics.twoDimGraphics.Closable
import kotlinx.cinterop.CPointer

public actual class SvgSurface private constructor(ptr: CPointer<cairo_surface_t>?) : Surface {
    public override val surfacePtr: CPointer<cairo_surface_t>? = ptr
    override val isClosed: Boolean
        get() = _isClosed
    private var _isClosed = false

    public actual companion object {
        public actual fun create(
            filePath: String,
            width: Double,
            height: Double
        ): SvgSurface = SvgSurface(
            cairo_svg_surface_create(filename = filePath, width_in_points = width, height_in_points = height)
        )
    }

    /**
     * Restricts the generated SVG file to version. This function should only be called **BEFORE** any drawing
     * operations have been performed on the given surface. The simplest way to do this is to call this function
     * immediately after creating the surface.
     * @param version The SVG version to use.
     */
    public fun restrictToVersion(version: _cairo_svg_version) {
        if (isClosed) throw IllegalStateException("SvgSurface is closed.")
        cairo_svg_surface_restrict_to_version(surfacePtr, version)
    }

    override fun close() {
        if (!isClosed) cairo_surface_destroy(surfacePtr)
        _isClosed = true
    }
}