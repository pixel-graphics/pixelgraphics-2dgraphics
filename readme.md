# Pixel Graphics 2D Graphics (pixelgraphics-2dgraphics)

A Kotlin Native library that provides 2D graphics functionality (eg 2D drawing), which is based on the
[Cairo](https://www.cairographics.org/) library v1.10 (required by the Pixel Graphics 2D Graphics library). This 
library depends on Kotlin Native (requires Kotlin 1.5.21) which is currently in beta, and doesn't provide any backwards 
compatibility guarantees!

## Setup Gradle Build File

In order to use the library with Gradle (version 6.7 or higher) do the following:

1. Open/create a Kotlin Native project which targets **linuxX64** or **linuxArm32Hfp**
2. Open the project's **build.gradle.kts** file
3. Insert the following into the **repositories** block:
```kotlin
mavenCentral()
```
4. Create a library definition file called **cairo.def** which contains the following:
```
linkerOpts = -lcairo
linkerOpts.linux_x64 = -L/usr/lib/x86_64-linux-gnu
linkerOpts.linux_arm32_hfp = -L/mnt/pi_image/usr/lib/arm-linux-gnueabihf
```

5. Add the Cairo library dependency: `cinterops.create("cairo")`
6. Add the Pixel Graphics 2D Graphics library dependency: 
   `implementation("io.gitlab.pixel-graphics:pixelgraphics-2dgraphics:$pixelGraphicsVer")`

The build file should look similar to the following:
```kotlin
// ...
repositories {
    mavenCentral()
}

kotlin {
    // ...
    linuxX64 {
        // ...
        compilations.getByName("main") {
            cinterops.create("cairo")
            dependencies {
                val pixelGraphicsVer = "0.1"
                implementation("io.gitlab.pixel-graphics:pixelgraphics-2dgraphics:$pixelGraphicsVer")
            }
        }
    }
}
```
